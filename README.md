# reddit_text_analysis

## to use this package

In order to use this package, you must have a file named `credentials.yaml` in your working directory. The contents of this file should be in this format:

	reddit:
		app_id: <app id>
		app_secret: <app secret>
		userpw: <user password>
		username: <reddit user name>
		useragent: <user agent for requests (no spoofing browsers)>
	database:
		server: 
		database: 
		username: 
		password: 



