
-- example showing DATEDIFF in MS SQL Server
-- will need to use this when determining when to "rerun" 

SELECT
	created_utc,
	added_utc,
	DATEDIFF(day, created_utc, added_utc) as days_diff
FROM [reddit].[post]
WHERE DATEDIFF(day, created_utc, added_utc) < 60;


