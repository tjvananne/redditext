
--CREATE SCHEMA reddit
--GRANT ALL TO tjvananne;
--USE reddit;


-- working with datetime2
-- http://www.sqlservertutorial.net/sql-server-basics/sql-server-datetime2/

-- create post_selftext table
CREATE TABLE [reddit].[post] (
	postid varchar(12) PRIMARY KEY,
	subreddit varchar(40) NOT NULL,
	title varchar(300) NOT NULL,
	--created_utc varchar(11) NOT NULL,   -- when this link/post was created by user
	created_utc smalldatetime NOT NULL,   -- when this link/post was created by user
	--added_utc varchar(11) NOT NULL,   -- when this link/post was captured and added to my database
	added_utc smalldatetime NOT NULL,   -- when this link/post was captured and added to my database
	media_link varchar(100),            -- this is actually a json object itself, will need to parse it properly
	author_fullname varchar(50) NOT NULL,
	has_selftext bit NOT NULL,
);

INSERT INTO [reddit].[post] (postid, subreddit, title, created_utc, added_utc, author_fullname, has_selftext)
VALUES ('t3_c0j0ex', 'shortstories', '(AA) All Men Must Fall', '1560511850', '1560511850', 'P0brien', 1);


truncate table [reddit].[post];


-- create post_selftext table
CREATE TABLE [reddit].[post_selftext] (
	postid varchar(12) FOREIGN KEY REFERENCES [reddit].post(postid),
	selftext varchar(max) NOT NULL,
);


-- create post_metrics table
CREATE TABLE [reddit].[post_metrics] (
	metricid int IDENTITY(1,1) PRIMARY KEY,
	postid varchar(12) FOREIGN KEY REFERENCES [reddit].post(postid),
	metric_utc smalldatetime NOT NULL,	--'2018-06-23 07:30:20' in utc as well?
	--metric_utc varchar(11) NOT NULL,	
	comment_count int,
	ups int,
	downs int,
);



/*
drop table reddit.post_metrics;
drop table reddit.post_selftext;
drop table reddit.post;
*/

