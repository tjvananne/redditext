
-- ctrl + shift + r to refresh the intellisense local cache


SELECT TOP (1000) [metricid]
      ,[postid]
      ,[metric_utc]
      ,[comment_count]
      ,[ups]
      ,[downs]
  FROM [reddit].[post_metrics]
  ORDER BY postid, metricid;


  -- setting up the lag variable to do the metric time delta calculation
SELECT 
	   [metricid]
	  ,LAG(metricid, 1) OVER(Partition by postid Order by postid, metricid) as last_metricid
      ,[postid]
      ,[metric_utc]
      ,[comment_count]
      ,[ups]
      ,[downs]
  FROM [reddit].[post_metrics];



-- interesting... ok we're getting a little closer
SELECT 
	a.metricid,
	a.postid,
	b.[metric_utc]
	FROM 
		(SELECT 
			max(metricid) as metricid,
			postid
		FROM reddit.post_metrics
		GROUP BY postid) a
	LEFT JOIN reddit.post_metrics b
	ON a.metricid = b.metricid;	  



SELECT 
	-- table a is our result of group by, where we find the last record per each postid
	a.metricid,
	a.postid,
	b.metric_utc,
	b.prev_metric_utc,
	datediff(minute, b.prev_metric_utc, b.metric_utc) as minute_delta 
	FROM 
		-- first find the "last" record per each postid
		(SELECT 
			max(metricid) as metricid,
			postid
		FROM reddit.post_metrics
		GROUP BY postid) a
	LEFT JOIN 
		-- next, shift metric_utc so that we can find the delta between the latest record and the latest-1 record
		(SELECT 
			metric_utc,
			metricid,
			LAG(metric_utc, 1) OVER(Partition by postid Order by postid, metricid) as prev_metric_utc
		FROM reddit.post_metrics) b
	ON a.metricid = b.metricid;	  


SELECT 
	   [metricid]
	  ,LAG(metricid, 1) OVER(Partition by postid Order by postid, metricid) as last_metricid
      ,[postid]
      ,[metric_utc]
      ,[comment_count]
      ,[ups]
      ,[downs]
  FROM [reddit].[post_metrics] m1
  left join 
  SELECT
  metricid as thismetricid,
  postid as thispostid,
  metric_utc as thismetricutc
  FROM [reddit].[post_metrics] m2
  ON m1.metricid = m2.metricid;


  -- identify the "last record" per each postid
SELECT
	   max(m2.[metricid]) as metricid
	  ,m2.[postid]
	  ,LAG(m1.metricid, 1) OVER(Partition by m1.postid Order by m1.postid, m1.metricid) as last_metricid
FROM [reddit].[post_metrics] m2
left join 
(SELECT 
	   [metricid]
	  ,LAG(metricid, 1) OVER(Partition by postid Order by postid, metricid) as last_metricid
      ,[postid]
      ,[metric_utc]
      ,[comment_count]
      ,[ups]
      ,[downs]
  FROM [reddit].[post_metrics]) m1
ON m1.metricid = m2.metricid
GROUP BY m2.[postid];



--truncate table [reddit].post_metrics;

