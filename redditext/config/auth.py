

# https://www.reddit.com/prefs/apps
# https://www.reddit.com/dev/api


# standard
from datetime import datetime
import json
import os
import time

# third party
import yaml
import requests
from requests.auth import HTTPBasicAuth


class AuthObj():
	"""
	This is the object you pass all of the requests to for authentication
	TODO: need to implement how long the token is valid so we can check (whole point of this class)
	TODO: need to implement the notion of "scope" to check against the api itself? (or let API do that?)
	"""
	def __init__(self):
		# constructor now requires the filepath to YAML credentials 
		self._last_requested = datetime(1970, 1, 1)
		self.allow_reauth = True
		self._access_token = None
		self._useragent = None
		self._token_type = None
		self._token_expires_in = 0  # I believe this is in seconds (default 3600)
		self._token_scope = None
		self._auth_string = None
		self._headers = {}
	
	
	def authenticate(self):
		"""
		This function reads your credentials from a yaml file and returns
		a python dictionary that can then be used for requesting an access token
		from the Reddit API.
		
		filepath (str): the file path of where your yaml credentials are stored.
		
		example filepath value: "C:/users/myusername/documents/credentials.yaml"
		
		YAML file should be structured as so:
		
		# start of yaml file:
		reddit:
			app_id: <your app id here>
			app_secret: <your app secret here>
			username: <your reddit username here>
			userpw: <your reddit password here>
			useragent: <your user agent definition here>
		
		see the Readme.md file for this package if you need help understanding
		what each of the above elements of the yaml file represent.
		
		"""
		
		with open(self.filepath) as f:
			credentials = yaml.safe_load(f)
		
		# parse yaml credentials file
		reddit_creds = credentials.get('reddit')
		app_id = reddit_creds.get('app_id')
		app_secret = reddit_creds.get('app_secret')
		username = reddit_creds.get('username')
		userpw = reddit_creds.get('userpw')
		useragent = reddit_creds.get('useragent')
		
		# auth object for app id and secret (--user in curl)
		authobj = HTTPBasicAuth(app_id, app_secret)

		# data / parameters (-d in curl)
		params = {
			'grant_type':'password',
			'username':username,
			'password':userpw
		}

		# header for user agent
		self._useragent = useragent
		headers = {'User-agent': self._useragent}


		# make request for token
		resp = requests.post("https://www.reddit.com/api/v1/access_token",
							 data=params,
							 headers=headers,
							 auth=authobj)
		
		resp_content = resp.json()
		print(resp_content)
		self._last_requested = datetime.now()
		self._access_token = resp_content.get('access_token')
		self._token_type = resp_content.get('token_type')
		self._token_expires_in = resp_content.get('expires_in')
		self._token_scope = resp_content.get('scope')
		self._auth_string = f'{self._token_type} {self._access_token}'
		self._headers = {
			'Authorization': self._auth_string,
			'User-agent': self._useragent}
		print("Successfully Authenticated. Sleeping two seconds")
		time.sleep(2)



if __name__ == "__main__":
	myauth = AuthObj()
	print("initial timestamp: ", myauth.last_requested)
	myauth.authenticate("C:/users/tjvan/documents/github_repos/reddit_text_analysis/credentials.yaml")
	
	print("access_token: ", myauth.access_token)
	print("new timestamp: ", myauth.last_requested)


