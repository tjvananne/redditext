

# standard
import re
import platform
from datetime import datetime, timedelta

# third party
import pyodbc 
import yaml



class DatabaseObj(object):
	
	def __init__(self, filepath):
	
		with open(filepath, 'r') as f:
			credentials = yaml.safe_load(f)

		dbcreds = credentials.get('database')

		# https://docs.microsoft.com/en-us/sql/connect/python/pyodbc/step-3-proof-of-concept-connecting-to-sql-using-pyodbc?view=sql-server-2017
		
		self._server = dbcreds.get('server') 
		self._database = dbcreds.get('database') 
		self._username = dbcreds.get('username')
		self._password = dbcreds.get('password')
		self.cursor = None
		self.cnxn = None
		self.query = None
		self.db_authenticate()
	
	
	def __single_quote_cleaner(self, text):
		"""
		For now, we'll keep this a simple as possible and
		simply remove all single quotes from the text. We'll 
		replace them with double quotes instead.
		"""
		pattern1 = re.compile("'")
		text = pattern1.sub("", text)
		return text
	
	
	def db_authenticate(self):
		
		"""
		TODO: not sure how platform independent this all is...
		If required, might need to add a call to platform.system() to determine if we're on windows or Linux
		"""
		self.cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self._server+ \
			';DATABASE='+self._database+';UID='+self._username+';PWD='+self._password)
		cursor = self.cnxn.cursor()
		print("successfully authenticated to database")
		self.cursor = cursor
		
		
	def build_query_single_row_insert(self, schema, table, **kwargs):
    
		"""
		This function builds the query from a schema, tablename, and a kwargs dictionary
		that represents {column-name: value} pairs.
		
		It's very generic. It should work with any schema, table, kwargs.
		TODO: check what types of exceptions are raised when something fails at 
		the database level while using pyodbc. Such as failing to include a
		column/value combo for something that is NOT NULL in the db
		"""
		
		self.query = None
		
		# initialize query, colnames string, and values string
		qry = f"INSERT INTO [{schema}].[{table}] "
		colnames = "("
		values   = " VALUES ("
		
		# loop through our kwargs dict for what to add to the table
		for key, value in kwargs.items():

			# if it's a string, we want to add our single quotes in front and back
			if isinstance(value, str):
				value = self.__single_quote_cleaner(value)
				value = "'" + value + "'"
				# maybe also do some sort of regex check for single ' ' within the string
				# then replace them with two ''
				# https://stackoverflow.com/questions/1586560/how-do-i-escape-a-single-quote-in-sql-server
			
			# if it isn't a string, let's make it one now so it behaves in our concatenation later
			else:
				value = str(value)
				
			# string concatenation to build out column names and values of query
			colnames = colnames + key + ", "
			values = values + value + ", "
			
		# remove the comma + space from the last iteration, add paren
		colnames = colnames[:-2] + ") "
		values = values[:-2] + ");"
		
		# build the final query
		qry = qry + colnames  + values
		print(qry, type(qry))
		self.query = qry


	def execute_query_single_row_insert(self):
		
		if self.query is None:
			# TODO: maybe raise an exception here instead of just print?
			print("No valid query has been defined yet. Nothing to execute.")
			
		self.cursor.execute(self.query)
		self.cnxn.commit()
			
			
	def execute_query_bulk_insert(self, schema, table, list_of_dicts):
		"""
		list_of_dicts: this is a list of dictionary objects, each dictionary
		object represents a row to be added to the database. The keys in the
		dictionary object represent the column names of the database table 
		while the values represent the values you want to add to those columns in
		the db table.
		"""
		row_count = 0
		for row in list_of_dicts:
			try:
				self.build_query_single_row_insert(schema=schema, table=table, **row)
				self.execute_query_single_row_insert()
				row_count += 1
			except pyodbc.Error as e:
				# TODO: I eventually want to log this out so I have a record
				# of all of the rows I tried to add but that failed
				print("Database Integrity error: ", e)
		
		# return the number of rows added to the database so we can choose
		# whether to execute another "page" of results or not
		return row_count





