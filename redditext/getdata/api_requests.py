

# stuff I need for these classes:
from urllib.parse import urljoin
from datetime import datetime, timedelta
import requests
import time

# internal
from ..config.auth import AuthObj


"""
I want to have a RedditApiBase() class, then create downstream classes
that inherit from this base class:

RedditApiBase should have the "send_request()" method instead of implementing
that for each downstream class

1) a RedditLinkCollector() class that handles collecting new links
"""


class RedditApiBase(AuthObj):

	def __init__(self, filepath):
	
		self.filepath = filepath
		self.base_url = "https://oauth.reddit.com"
		self.last_response = None
		self.authenticate()

		# initial check to see if client is valid - otherwise reauthenticate
		if self._access_token is None:
			print("No valid access token on client object, attempting to re-authenticate")
			self.authenticate()
				
	def __check_token_time(self):
		
		last_req = self._last_requested
		
		# (Magic number context) 90% of expire time as a buffer
		expires_in = round(self._token_expires_in * 0.9)
		
		# if last requested timestamp is greater than 
		# last requested timestamp + "expires in" amount
		is_expired = last_req > (last_req + timedelta(seconds=expires_in))
		if is_expired:
			print("Token appears to be expired, attempting to re-authenticate")
			self.authenticate()

	def _epoch_utc_to_datestr(self, utc_ts):
		"""
		pass in a a 10-digit epoch (seconds since 1970-01-01) that is meant
		to be in utc time. this will convert to a datestring in UTC that 
		MS SQL Server is able to ingest into a smalldatetime, datetime, or datetime2
		"""	
		epoch = datetime(1970, 1, 1, 0, 0, 0, 0)
		utc_datetime = epoch + timedelta(seconds=utc_ts)
		return str(utc_datetime.strftime("%Y-%m-%d %H:%M:%S"))


	def _utc_now_datestr(self):
		_utcnow = datetime.utcnow()
		return str(_utcnow.strftime("%Y-%m-%d %H:%M:%S"))


	def send_request(self, req_str, **kwargs):
		
		"""
		Could also call this method with a dictionary of items
		https://stackoverflow.com/questions/5710391/converting-python-dict-to-kwargs
		This might be more useful once we get into adding several datapoints into
		a single row in the database. That would be in a separate database-load method
		"""
		
		self.last_response = None
		params = dict()
		for key, value in kwargs.items():
			params[key] = value
		
		print(params)
				
		# before sending off request, need to check if token is expired
		self.__check_token_time()
		
		# combine base url with what the user wants returned
		full_url = urljoin(self.base_url, req_str)
		print("Sending request: ", full_url)
		resp = requests.get(full_url, headers=self._headers, params=params)
		self.last_response = resp.json()
		# print(self.last_response)
		time.sleep(2)

	def time_since_obj(self, p_ts):
		"""
		p_ts: this is the UTC timestamp of the object we're checking. It should be
		something like "created_utc" inside of the response object from reddit
		
		This function will then return the time since this object
		was created on Reddit (in HOURS).
		"""
		utc_now = datetime.utcnow()
		utc_posted = datetime.utcfromtimestamp(p_ts)
		td = utc_now - utc_posted
		hours = td.seconds / 60 / 60
		return hours
		

class RedditApiListing(RedditApiBase):
	
	def __init__(self, filepath):
		
		# this logic lives on the parent class, call that version of init
		super().__init__(filepath)
	
	def parse_media(self, media_json):
		"""
		Some of these listings will have a media attribute. I thought it would be
		just a single link and I could add it to the db, but it's actually a
		json object itself. I need to collect a few different types of these and
		build this into a generic method that can handle many of them.
		"""
		pass
	

	def build_dict_link_listing(self):
		"""
		TODO: need this method to query the database to identify any of the
		titles (or ID or whatever the primary key is) that already exist.
		If it exists in the database already, then we want to filter it out
		of here. It would be useful to include logic based on that result
		that would guide whether we want to continue "paging" through some
		of these results or not... Need to go through the exercise of walking
		through the reddit /new/ results to see how this should work
		
		This will build out a list of dictionaries to be passed into the 
		DatabaseObj object.
		"""
		
		# isolate the list of children
		children = self.last_response.get('data').get('children')
		list_of_child_dict = []      # this is for the post table
		list_of_selftext_dict = []   # this is for the post_selftext table
		list_of_metrics_dict = []    # this is for the metrics table
		print(type(children))
		
		for i, child in enumerate(children):
			child = child.get('data')
			postid = child.get("name")
			subreddit = child.get("subreddit")
			title = child.get("title")
			created_utc = self._epoch_utc_to_datestr(child.get("created_utc"))
			added_utc = self._utc_now_datestr()
			# media_link = child.get("media")  # so this can be a json object... currently not parsing that
			author_fullname = child.get("author")
			has_selftext = child.get("is_self")
			selftext = child.get("selftext")
			
			# doesn't count as selftext if the self text is empty... at least I don't count it as selftext
			if has_selftext and selftext.strip() != "": 
				has_selftext = 1
				
				_this_selftext_dict = {
					"postid": postid,
					"selftext": selftext
				}
				list_of_selftext_dict.append(_this_selftext_dict)
				
			else:
				has_selftext = 0
			
			_this_dict = {
				"postid": postid,  # as postid
				"subreddit": subreddit,
				"title": title,
				"created_utc": created_utc,
				"added_utc": added_utc,
				# "media_link": media_link, # as media_link
				"author_fullname": author_fullname, # as author_fullname
				"has_selftext": has_selftext # as has_selftext
			}
			
			# remove the dict items where the value is None
			_this_dict_valid = {k: v for k, v in _this_dict.items() if v is not None}
			
			list_of_child_dict.append(_this_dict_valid)
			
		
			_this_metric_dict = {
				"postid": postid,
				"metric_utc": self._utc_now_datestr(),
				"comment_count": child.get('num_comments'),
				"ups": child.get('ups'),
				"downs": child.get('downs')
			}
			list_of_metrics_dict.append(_this_metric_dict)
			
		
		return [list_of_child_dict, list_of_selftext_dict, list_of_metrics_dict]


